"""
Test for the rezPackageTemplate tool
"""

from rezPackageTemplate import my_tool
import unittest


class KnownValues(unittest.TestCase):
    known_values = ((('Mario', 1), 'my_tool tool_name Mario 1'),
                    (('Luigi', 2), 'my_tool tool_name Luigi 2'),
                    (('Peach', 3), 'my_tool tool_name Peach 3'),
                    (('Browser', 4), 'my_tool tool_name Browser 4'),
                    (('Maskass', 5), 'my_tool tool_name Maskass 5'))

    def test_my_tool_tool(self):
        for args, know_value in self.known_values:
            result = my_tool.MyTool('tool_name').tool(*args)
            self.assertEqual(know_value, result)


def run_test():
    """
    Run the test
    """
    unittest.main()
