# package.py

This file is a template Rez package with the common configuration options used at ArtFX

For more information, go to : https://github.com/nerdvegas/rez/wiki/Package-Definition-Guide
```python
#  Copyright (c) ArtFx 2021.

# The name of the package (mandatory)
name = "test-package"

# The version (prefer semantic versioning)
version = "1.0.0"

# Description, it should not mention details about a particular version
description = "This is a template Rez package"

# A list of help links
help = ["https://github.com/nerdvegas/rez/wiki", "https://docs.python.org/3/"]

# A list of variants
# variants = [["test-2020"], ["test-2021"]]

# Authors of the package, optional
authors = ["Alice", "Bob"]

# The list of binary executables that can be run within that environment
tools = ["test", "tool"]

# Or use a function to get the tools from the bin folder
# @early()
# def _tools():
#     """
#     Get the list of binaries inside the bin folder
#     """
#     import os

#     return os.listdir("./bin")

# List of packages to require
# For the syntax : https://github.com/nerdvegas/rez/wiki/Basic-Concepts#package-requests
requires = ["python-3.7+"]

# Enable package caching (we rely on shared storage)
# WARN : you should not enable caching on package repositories where packages may get overwritten
cachable = True

# Indicates that the package is an application that may have plugins
# has_plugins = True

# Provided if this package is a plugin of another package
# plugin_for = "maya"

# ------------------------ TESTS ------------------------

# Run tests with the rez-test command
# See : https://github.com/nerdvegas/rez/wiki/Package-Definition-Guide#tests
# tests = {
#     "unit": "python -m unittest discover -s {root}/python/tests",
#     "lint": {
#         "command": "pylint mymodule",
#         "requires": ["pylint"],
#         "run_on": ["default", "pre_release"]
#     }
# }

# ------------------------ BUILD ------------------------

# This defines the minimum version of rez needed to build this package
requires_rez_version = "2.70"

# Requirements for the build
# build_requires = [
#     "cmake-2.8",
#     "doxygen"
# ]

# If present, this is used as the build command when rez-build is run
build_command = False
# build_command = "bash {root}/build.sh {install}"

# Specify the build system used to build this package.
# Can also be set using the --build-system option
# build_system = "cmake"


# ------------------------ FUNCTIONS ------------------------


def commands():
    """
    Add paths to the environment, so the package can be used
    """
    global env
    env.PATH.append("{root}/bin")
    env.PYTHONPATH.append("{root}/lib")

    # Do something if building...
    # if building:
    #    pass

uid = '4216b4bdcc074930b663d097fa315818'
# def uuid():
#     """
#     Automatically generate a uuid for the package
#     From : https://github.com/nerdvegas/rez/wiki/Package-Definition-Guide#uuid
#     """
#     import uuid
#     return uuid.uuid4().hex

```