# ------------------------ DESCRIPTION ------------------------

name = "rezPackageTemplate"

version = "1.0.0"

@early()
def description():
    desc = """
        ---------- %s version %s ----------
        This is a template Rez package
    """ % (this.name, this.version)
    return desc


help = [
    "https://github.com/nerdvegas/rez/wiki",
    "https://docs.python.org/3/",
]

authors = [
    "ArtFx Dev team"
]


@early()
def _tools():
    """
    Get the list of binaries inside the bin folder
    """
    import os
    return os.listdir("./bin") if os.path.exists("./bin") else []


requires = [
    "python-3.7+"
]

# Enable package caching (we rely on shared storage)
# WARN : you should not enable caching on package repositories where packages may get overwritten
cachable = True

vcs = "git"

# ------------------------ TESTS ------------------------

def pre_test_commands():
    if test.name == "unit":
        env.IS_UNIT_TEST = 1
        env.PYTHONPATH.append("{root}/tests")

# Run tests with the rez-test command
tests = {
    "unit": {
        "command": "python -m unittest discover -s {root}/tests",
        "run_on": ["default", "pre_release"]
    }
}

# ------------------------ BUILD ------------------------

build_command = "python {root}/build.py {install}"

# ------------------------ FUNCTIONS ------------------------

def commands():
    """
    Add paths to the environment, so the package can be used
    """
    global env
    env.PATH.append("{root}/bin")
    env.PYTHONPATH.append("{root}/python")

def uuid():
    """
    Automatically generate a uuid for the package
    From : https://github.com/nerdvegas/rez/wiki/Package-Definition-Guide#uuid
    """
    import uuid
    return self.uuid if self.uuid else uuid.uuid4().hex
