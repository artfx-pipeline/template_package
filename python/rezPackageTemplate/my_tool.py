#  Copyright (c) ArtFx 2021.

class MyTool:
    """
    MyTool class description
    """

    def __init__(self, name):
        """
        MyTool constructor
        :param str name: name of the tool
        """
        self.name = name

    def tool(self, arg1, arg2, arg3=None):
        """
        This is my tool comment.

        :param str arg1: First argument
        :param int arg2: Second argument
        :param dict arg3: Optional argument
        :return str: tool name
        """
        return "my_tool {} {} {}".format(self.name, arg1, arg2)

    def addition(self, a, b):
        """
        Addition a with b

        :param int a: number to addition
        :param b: number to addition
        :return int: the addition off a with b
        """
        return a + b
